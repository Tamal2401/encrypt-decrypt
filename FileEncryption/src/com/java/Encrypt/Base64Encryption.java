package com.java.Encrypt;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

import org.apache.commons.codec.binary.Base64;

public class Base64Encryption {

	public static void main(String[] args) {

		File file = new File("C:\\Users\\TAPU\\Desktop\\1.txt");

		try
		{
			// Reading a Image file from file system
			FileInputStream imageInFile = new FileInputStream(file);
			byte imageData[] = new byte[(int) file.length()];
			imageInFile.read(imageData);

			// Converting Image byte array into Base64 String
			String imageDataString = encodeImage(imageData);

			// Converting a Base64 String into Image byte array
			byte[] imageByteArray = decodeImage(imageDataString);

			// Write a image byte array into file system
			FileOutputStream imageOutFile = new FileOutputStream(
					"C:\\Users\\TAPU\\Desktop\\converted.jpg");

			imageOutFile.write(imageByteArray);

			imageInFile.close();
			imageOutFile.close();

			System.out.println("Image Successfully Manipulated!");
		}catch(FileNotFoundException e){
			System.out.println("Image not found" + e);
		}catch(IOException ioe){
			System.out.println("Exception while reading the Image " + ioe);
		}
	}

	private static byte[] decodeImage(String imageDataString) {
		return Base64.decodeBase64(imageDataString);
	}

	private static String encodeImage(byte[] imageData) {
		return Base64.encodeBase64URLSafeString(imageData);
	}
}
