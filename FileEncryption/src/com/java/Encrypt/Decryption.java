package com.java.Encrypt;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

@SuppressWarnings("resource")
public class Decryption extends HelperLogicUtil {

	public static void main(String[] args) {

		Scanner scn = new Scanner(System.in);
		String fileName = scn.nextLine();
		String pwd = scn.nextLine();

		String toBeEncryptFolder = ENCRYPTED_Folder;
		String resourcePath = getResourcesPath(toBeEncryptFolder, fileName);

		File imageFile = new File(resourcePath);

		try {
			// Reading a Image file from file system
			FileInputStream imageInFile = new FileInputStream(imageFile);
			byte imageData[] = new byte[(int) imageFile.length()];
			imageInFile.read(imageData);

			// Converting Image byte array into Base64 String
			String imageDataString = new String(imageData);

			// decrypting the encrypted file into String
			String encryptedString = decrypt(imageDataString, pwd);

			// Base64 decoding the String to bytestream
			byte[] imageByteArray = decodeImage(encryptedString);

			// Converting the file and saving it in Decrypted folder
			SaveDecryptedImageToFolder(imageByteArray, fileName);

			System.out.println("File Decryption successfull");

			imageInFile.close();
		} catch (Exception e) {
			System.out.println(
					"Error while DECRYPTING the file and saving it in class path with error code :: " + e.getMessage());
		}
	}

	private static void SaveDecryptedImageToFolder(byte[] imageByteArray, String fileName) throws Exception {

		if (null != imageByteArray) {
			FileOutputStream imageOutFile = new FileOutputStream(getResourcesPath(DECRYPTED_Folder, fileName));
			imageOutFile.write(imageByteArray);
		} else {
			throw new Exception("ByteStream is null");
		}
	}
}
