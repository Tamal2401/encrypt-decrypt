package com.java.Encrypt;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

@SuppressWarnings("resource")
public class Encryption extends HelperLogicUtil {

	/*
	 * Main Encryption Method
	 */
	public static void main(String[] args) {

		Scanner scn = new Scanner(System.in);
		String fileName = scn.nextLine();
		String pwd = scn.nextLine();

		String toBeEncryptFolder = BASE_Folder;
		String resourcePath = getResourcesPath(toBeEncryptFolder, fileName);

		File imageFile = new File(resourcePath);
		try {
			// Reading a Image file from file system
			FileInputStream imageInFile = new FileInputStream(imageFile);
			byte imageData[] = new byte[(int) imageFile.length()];
			imageInFile.read(imageData);

			// Converting Image byte array into Base64 String
			String imageDataString = encodeImage(imageData);

			// Encrypting the image using AES algorithm
			String encryptedString = encrypt(imageDataString, pwd);

			// saving the image to encrypted folder
			saveImageToPath(encryptedString, fileName);
			System.out.println("File encryption Successfull");
			imageInFile.close();

			System.out.println("Image Successfully Manipulated!");
		} catch (Exception e) {
			System.out.println("Error while ENCRYPTING the file and saving it in class path with error code :: " + e.getMessage());
		}
	}

	private static void saveImageToPath(String encryptedString, String fileName) throws Exception {

		if (null != encryptedString) {
			FileOutputStream imageOutFile = new FileOutputStream(getResourcesPath(ENCRYPTED_Folder, fileName));
			byte[] byteArray = encryptedString.getBytes();
			imageOutFile.write(byteArray);
		} else {
			throw new Exception("encrypted string is null");
		}

	}
}
